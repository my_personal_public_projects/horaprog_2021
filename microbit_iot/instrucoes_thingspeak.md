## Introdução ##

O Kit IoT permite construir uma série de experiências onde o participante irá produzir um conjunto de dados proveniente de uma rede sensorial e, posteriormente, enviar estes dados, em tempo real, para a cloud onde poderão ser consutados por todos.

A primeira etapa é a criação de uma conta na [ThingSpeak](https://thingspeak.com/)

***

## Na plataforma ThingSpeak"

Para aceder à plataforma thingSpeak clique [aqui](https://thingspeak.com/)

1. Clique no icon de login da plataforma

![Img1](./imgs/001.png)

2. Clique no link "Create One"

![Img1](./imgs/002.png)

3. Insira os dados solicitados para a criação da conta e clique em "Continue".

![Img1](./imgs/003.png)

4. Vá à sua caixa de entrada de correio eletrónico, sem fechar a janela do thingspeak e confirmar a sua identidade.

![Img1](./imgs/004.png)

5. Depois de validar a sua identidade, volte à janela anterior e clique en "Continue"

![Img1](./imgs/005.png)

6. Crie uma password obedecendo aos critérios, depois clique em "Continue"

![Img1](./imgs/006.png)

7. Aceite os termos e condições e clique em "Continue"

![Img1](./imgs/007.png)

8. Nesta fase deverá aparacer uma mensagem que a sua conta foi criada com sucesso

![Img1](./imgs/008.png)

9. Vá à opção "Channels -> My Channels" e clique em "New Channel"

![Img1](./imgs/009.png)

10. Dê um nome ao canal e ative todos os 8 campos disponiveis dando os nomes que estão na figura.

![Img1](./imgs/010.png)

11. Clique em "Save Channel"

![Img1](./imgs/011.png)

12. Vá ao seu canal, ao tabulador API Keys e registe a API Key do seu canal.

![Img1](./imgs/012.png)

Os passos necessários na plataforma thingspeak estão concluidos
