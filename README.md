## HoraProg 2021

Este projeto tem como objetivo a preparação de atividades no âmbito do evento HORAPROG-2021
levado a cabo pela Universidade dos Açores.

Este evento tem por base a abertura da universidade a todas as escolas e população em geral
onde são realizados um conjunto de workshops dos mais variados temas relacionados com as
tecnologias de informação.

Este repositório tem por base o registo e partilha dos projetos ligados aos workshops de
Domótica, Robótica e IoT

## O Microbit ##

Nesta edição, iremos utilizar o microcontorlador MicroBit em todos os workshops apontados
acima.

Os kits utilizados são da ElecFreaks e da WaveShare. Os manuais e instruções com várias experiências didáticas podem ser encontrados aqui: [Clique para aceder](http://www.elecfreaks.com/learn-en/)

Para os workshops vamos utilizar:

1. Workshops Domotica: [SmartHome Kit](http://www.elecfreaks.com/learn-en/microbitKit/smart_home_kit/index.html)

2. Workshops IoT: [Smart Science IoT Kit](https://elecfreaks.com/learn-en/microbitKit/iot_kit/index.html)

3. WorkShops Robótica: [KitiBot-MG-T Wiki](https://www.waveshare.com/wiki/KitiBot_for_micro:bit), [KitiBot-MG-T Manual](https://www.waveshare.com/w/upload/7/7d/AlphaBlock_User_Manual_EN.pdf)



É necessário fazer um upgrade de firmware ao microcontrolador para que seja reconhecido
pela plataforma de programação em blocos.

***

**Instruções para upgrade de software**


1. Download do firmware para a versao 1.x do microcontrolador [Download](https://cdn.sanity.io/files/ajwvhvgo/production/26b176670c8f9df4aa015e9067368eafcfd749a1.hex?dl=0249_microbit_firmware.hex).

2. Abra o explorador de ficheiros que no linux ou windows.

3. Com o botão de reset do microcontrolador pressionado ligue o cabo USB ao mesmo. O LED amarelo, ao lado da porta USB, irá piscar e o dispositovo irá aparacer como drive USB com o nome MAINTENANCE.

4. Arraste o ficheiro de firware para dentro do drive MAINTENANCE.

5. Nesta altura o microcontrolador faz o upgrade de firmware e passa a ser apresentado no explorador de ficheiros com o nome MICROBIT

**nota** Neste repositório temos uma cópia do firmware à data de 21 de nov, contudo é sempre conveniente, verificar 
a existência de novas versões de firmware, pode faz-se no seguinte link: [Clique para aceder](https://microbit.org/get-started/user-guide/firmware/)

***

## Este Repositório ##

Este repositório está estruturado com 3 pastas distintas:

1. **microbit_robotica**: Nesta pasta estão contidos os ficheiros HEX para as diferentes tarefas propostas para o workshop que uitiliza o kit de robótica Kitty. [Link para Wiki](https://www.waveshare.com/wiki/KitiBot_for_micro:bit)

Na plataforma microbit da Microsoft devemos adicionar a extensão, para os workshops de robótica, introduzindo o URL na pesquisa de extensões: **Extensão APA102: https://github.com/waveshare/pxt-APA102**, **Extensão KitiBot: https://github.com/waveshare/pxt-KitiBot**

2. **microbit_domotica**: Nesta pasta estão contidos os ficheiros HEX para as diferentes tarefas propostas para o workshop que uitiliza o kit de Domótica. [Link para as intruções do kit](https://www.elecfreaks.com/learn-en/microbitKit/smart_home_kit/smart_home_kit.html). 

3. **microbit_iot**: Nesta pasta estão contidos os ficheiros HEX para as diferentes tarefas propostas para o workshop que uitiliza o kit de IoT. [Link para as intruções do kit](https://elecfreaks.com/learn-en/microbitKit/iot_kit/index.html)

***






